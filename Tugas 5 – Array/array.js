//Soal No. 1 (Range)
console.log("Soal No. 1 (Range)");

function range(startNum, finishNum) {
  if (startNum < finishNum) {
    var angka = [];
    for (var i = startNum; i <= finishNum; i++) {
      angka.push(i);
    }
    return angka;
  } else if (startNum > finishNum) {
    var angka = [];
    for (var i = finishNum; i <= startNum; i++) {
      angka.push(i);
    }
    return angka.sort(function (startNum, finishNum) {
      return finishNum - startNum;
    });
  } else if (!finishNum) {
    return -1;
  }
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

console.log("\n");

//Soal No. 2 (Range with Step)
console.log("Soal No. 2 (Range with Step)");
function rangeWithStep(startNum, finishNum, step) {
  if (startNum < finishNum) {
    var angka = [];
    while (startNum <= finishNum) {
      angka.push(startNum);
      startNum += step;
    }
    return angka;
  } else if (startNum > finishNum) {
    var angka = [];
    while (startNum >= finishNum) {
      angka.push(startNum);
      startNum -= step;
    }
    return angka;
  }
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
console.log("\n");

//Soal No. 3 (Sum of Range)
console.log("Soal No. 3 (Sum of Range)");
function sum(startNum, finishNum, step) {
  if (!step) {
    step = 1;
  }

  if (startNum < finishNum) {
    var angka = [];
    var total = 0;
    while (startNum <= finishNum) {
      angka.push(startNum);
      total += startNum;
      startNum += step;
    }
    return total;
  } else if (startNum > finishNum) {
    var angka = [];
    var total = 0;
    while (startNum >= finishNum) {
      angka.push(startNum);
      total += startNum;
      startNum -= step;
    }
    return total;
  } else if (!startNum && !finishNum) {
    return 0;
  } else if (!finishNum) {
    return 1;
  }
}
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); //90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log("\n");

//Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4 (Array Multidimensi)");
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling(input) {
  for (var i = 0; i < input.length; i++) {
    console.log("Nomor ID: " + input[i][0]);
    console.log("Nama Lengkap: " + input[i][1]);
    console.log("TTL: " + input[i][2] + " " + input[i][3]);
    console.log("Hobi: " + input[i][4]);
    console.log("\n");
  }
}
dataHandling(input);
console.log("\n");

//Soal No. 5 (Balik Kata)
console.log("Soal No. 5 (Balik Kata)");

function balikKata(input) {
  var kata = [];
  panjang = input.length - 1;
  while (panjang >= 0) {
    akhir = input[panjang];
    kata.push(akhir);
    panjang -= 1;
  }

  return kata;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I
console.log("\n");

//Soal No. 6 (Metode Array)
console.log("Soal No. 6 (Metode Array)");
var input = [
  "0001",
  "Roman Alamsyah",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
function dataHandling2(input) {
  input.splice(1, 1, "Roman Alamsyah Elsharawy");
  input.splice(2, 1, "Provinsi Bandar Lampung");
  input.splice(4, 0, "Pria");
  input.splice(5, 1, "SMA Internasional Metro");
  console.log(input);

  var month = input[3].split("/");
  var bulan = month[1];

  switch (bulan) {
    case "01": {
      console.log("Januari");
      break;
    }
    case "02": {
      console.log("Februari");
      break;
    }
    case "03": {
      console.log("Maret");
      break;
    }
    case "04": {
      console.log("April");
      break;
    }
    case "05": {
      console.log("Mei");
      break;
    }
    case "06": {
      console.log("Juni");
      break;
    }
    case "07": {
      console.log("Juli");
      break;
    }
    case "08": {
      console.log("Agustus");
      break;
    }
    case "09": {
      console.log("September");
      break;
    }
    case "10": {
      console.log("Oktober");
      break;
    }
    case "11": {
      console.log("November");
      break;
    }
    case "12": {
      console.log("Desember");
      break;
    }
  }

  month.sort(function (value1, value2) {
    return value2 - value1;
  });
  console.log(month);

  month = input[3].split("/");
  var slug = month.join("-");
  console.log(slug);

  //console.log(input[1]);

  var irisan = input[1].slice(0, 15);
  console.log(irisan);
}
dataHandling2(input);

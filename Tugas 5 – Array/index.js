//.length
console.log(".length");
var hobbies = ["coding", "cycling", "climbing", "skateboarding"];
console.log(hobbies); // [ 'coding', 'cycling', 'climbing', 'skateboarding' ]
console.log(hobbies.length); // 4

console.log(hobbies[0]); // coding
console.log(hobbies[2]); // climbing
// Mengakses elemen terakhir dari array
console.log(hobbies[hobbies.length - 1]); //skateboarding

console.log("\n");

//
var feeling = ["dag", "dig"];
feeling.push("dug"); // menambahkan nilai "dug" ke index paling belakang
feeling.pop(); // menghapus nilai pada elemen terakhir array

//.push()
console.log(".push()");
var numbers = [0, 1, 2];
numbers.push(3);
console.log(numbers); // [0, 1, 2, 3]
// Bisa juga memasukkan lebih dari satu nilai menggunakan metode push
numbers.push(4, 5);
console.log(numbers); // [0, 1, 2, 3, 4, 5]

console.log("\n");

//.pop()
console.log(".pop()");
var numbers = [0, 1, 2, 3, 4, 5];
numbers.pop();
console.log(numbers); // [0, 1, 2, 3, 4]

console.log("\n");

//.unshift()
console.log(".unshift()");
var numbers = [0, 1, 2, 3];
numbers.unshift(-1);
console.log(numbers); // [-1, 0, 1, 2, 3]

console.log("\n");

//.shift()
console.log(".shift()");
var numbers = [0, 1, 2, 3];
numbers.shift();
console.log(numbers); // [1, 2, 3]

console.log("\n");

//.sort()
console.log(".sort()");
var animals = ["kera", "gajah", "musang"];
animals.sort();
console.log(animals); // ["gajah", "kera", "musang"]
console.log("\n");

var numbers = [12, 1, 3];
numbers.sort();
console.log(numbers); // [1, 12, 3]
console.log("\n");

var numbers = [12, 1, 3];
// Mengurutkan secara Ascending
numbers.sort(function (value1, value2) {
  return value1 - value2;
});
console.log(numbers); // [1, 3, 12]

// Mengurutkan secara Descending
numbers.sort(function (value1, value2) {
  return value2 - value1;
});
console.log(numbers); // [12, 3, 1]

console.log("\n");

//.slice()
console.log(".slice()");
var angka = [0, 1, 2, 3];
var irisan1 = angka.slice(1, 4);
console.log(irisan1); //[1, 2, 3]

var irisan2 = angka.slice(0, 3);
console.log(irisan2); //[0, 1, 2]

var angka = [0, 1, 2, 3];
var irisan3 = angka.slice(2);
console.log(irisan3); // [2, 3]

var angka = [0, 1, 2, 3];
var salinAngka = angka.slice();
console.log(salinAngka); // [0, 1, 2, 3]

console.log("\n");

//.splice()
console.log(".splice()");
var fruits = ["banana", "orange", "grape"];
fruits.splice(1, 0, "watermelon");
console.log(fruits); // [ "banana", "watermelon", "orange", "grape"]

var fruits = ["banana", "orange", "grape"];
fruits.splice(0, 2);
console.log(fruits); // ["grape"]

console.log("\n");

//.split() dan .join()
console.log(".split() dan .join()");
var biodata = "name:john,doe";
var name = biodata.split(":");
console.log(name); // [ "name", "john,doe"]

var title = ["my", "first", "experience", "as", "programmer"];
var slug = title.join("-");
console.log(slug); // "my-first-experience-as-programmer"

console.log("\n");

//Multidimensional Array
console.log("Multidimensional Array");
var arrayMulti = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];
// Maka sebagai gambaran, indeks dari array tersebut adalah
/*
    [
        [(0,0), (0,1), (0,2)],
        [(1,0), (1,1), (1,2)],
        [(2,0), (2,1), (2,2)]
    ] 
*/
console.log(arrayMulti[0][0]); // 1
console.log(arrayMulti[1][0]); // 4
console.log(arrayMulti[2][1]); // 8

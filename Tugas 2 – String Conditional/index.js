/**Hello world */
var sayHello = "Hello World!" 
console.log(sayHello)
console.log("\n") 

/**Data Type Variable */
var name = "John" // Tipe
var angka = 12
var todayIsFriday = false 

console.log("Data Type Variable")
console.log(name) // "John"
console.log(angka) // 12
console.log(todayIsFriday) // false
console.log("\n") 

/**Operator */
/**Equal Operator (==) */
console.log("Equal Operator")
var angka = 100
console.log(angka == 100) // true
console.log(angka == 20) // false
console.log("\n") 

/**Not Equal ( != ) */
console.log("Not Equal")
var sifat = "rajin"
console.log(sifat != "malas") // true
console.log(sifat != "bandel") //true
console.log("\n") 

/**Strict Equal ( === ) */
console.log("Strict Equal")
var angka = 8
console.log(angka == "8") // true, padahal "8" adalah string.
console.log(angka === "8") // false, karena tipe data nya berbeda
console.log(angka === 8) // true 
console.log("\n") 

/**Strict not Equal ( !== ) */
console.log("Strict not Equal")
var angka = 11
console.log(angka != "11") // false, padahal "11" adalah string
console.log(angka !== "11") // true, karena tipe datanya berbeda
console.log(angka !== 11) // false
console.log("\n") 

/**Kurang dari & Lebih Dari ( <, >, <=, >=) */
console.log("Kurang dari & Lebih Dari")
var number = 17
console.log( number < 20 ) // true
console.log( number > 17 ) // false
console.log( number >= 17 ) // true, karena terdapat sama dengan
console.log( number <= 20 ) // true
console.log("\n") 

/**OR ( || ) */
console.log("OR")
console.log(true || true); // true
console.log(true || false); // true
console.log(true || false || false); // true
console.log(false || false); // false
console.log("\n") 

/**AND ( && ) */
console.log("AND")
console.log(true && true); // true
console.log(true && false); // false
console.log(false && false); // false
console.log(false && true && true); // false
console.log(true && true && true); // true 
console.log("\n") 


/**String Properties & Method */
console.log("String Properties & Method")
var sentences = "Javascript" 
console.log(sentences[0]) // "J"
console.log(sentences[2]) // "v"
console.log("\n") 

/**String Properties */
console.log("String Properties")
var word = "Javascript is awesome"
console.log(word.length) // 21
console.log("\n")  

/**String Methods */
/**.charAt([indeks]) */
console.log(".charAt([indeks])")
console.log('i am a string'.charAt(3)); // 'm'
console.log("\n")  

/**.concat([string]) */
console.log(".concat([string])")
var string1 = 'good';
var string2 = 'luck';
console.log(string1.concat(string2)); // goodluck
console.log("\n")  

/**.indexOf([string/karakter]) */
console.log(".indexOf([string/karakter])")
var text = 'dung dung ces!';
console.log(text.indexOf('dung'));  // 0
console.log(text.indexOf('u'));     // 1
console.log(text.indexOf('jreng')); // -1
console.log("\n")  

/**.substring([indeks awal], [indeks akhir (optional)]) */
console.log(".substring([indeks awal], [indeks akhir (optional)])")
var car1 = 'Lykan Hypersport';
var car2 = car1.substr(6);
console.log(car2); // Hypersport
console.log("\n")  

/**.substr([indeks awal], [jumlah karakter yang diambil (optional)]) */
console.log(".substr([indeks awal], [jumlah karakter yang diambil (optional)])")
var motor1 = 'zelda motor';
var motor2 = motor1.substr(2, 2);
console.log(motor2); // ld
console.log("\n")  

/**.toUpperCase() */
console.log(".toUpperCase()")
var letter = 'This Letter Is For You';
var upper  = letter.toUpperCase();
console.log(upper); // THIS LETTER IS FOR YOU
console.log("\n")  

/**.toLowerCase() */
console.log(".toLowerCase()")
var letter = 'This Letter Is For You';
var lower  = letter.toLowerCase();
console.log(lower); // this letter is for you
console.log("\n") 

/**.trim() */
console.log(".trim()")
var username    = ' administrator ';
var newUsername = username.trim(); 
console.log(newUsername) // 'administrator'
console.log("\n") 

/**Mengubah tipe data dari dan ke String */
/**String([angka/array]) */
console.log("String([angka/array])")
var int  = 12;
var real = 3.45;
var arr  = [6, 7, 8];

var strInt  = String(int);
var strReal = String(real);
var strArr  = String(arr);

console.log(strInt);  // '12'
console.log(strReal); // '3.45'
console.log(strArr);  // '6,7,8'
console.log("\n") 

/**.toString() */
console.log(".toString()")
var number = 21;
console.log(number.toString()); // '21'
var array = [1,2];
console.log(array.toString());  // '1,2'
console.log("\n") 

/**Number([String]) */
console.log("Number([String])")
var number1 = Number("90");   // number1 = 90
console.log(number1) 
var number2 = Number("1.23"); // number2 = 1.23
console.log(number2) 
var number3 = Number("4 5");  // number3 = NaN
console.log(number3) 
console.log("\n") 

/**parseInt([String]) dan parseFloat([String]) */
console.log("parseInt([String]) dan parseFloat([String])")
var int  = '89';
var real = '56.7';
var strInt_1 = parseInt(int);  // strInt_1 = 89
console.log(strInt_1) 
var strInt_2 = parseInt(real); // strInt_2 = 56
console.log(strInt_2) 
var strReal_1 = parseFloat(int); // strReal_1 = 89
console.log(strReal_1) 
var strReal_2 = parseFloat(real); // strReal_2 = 56.7
console.log(strReal_2) 
console.log("\n") 
//Contoh Looping While-loop 1
console.log('Contoh Looping While-loop 1')

var flag = 1;
while(flag < 10) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log('Iterasi ke-' + flag); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}

console.log('\n')

//Contoh Looping While-loop 2
console.log('Contoh Looping While-loop 2')

var deret = 5;
var jumlah = 0;
while(deret > 0) { // Loop akan terus berjalan selama nilai deret masih di atas 0
  jumlah += deret; // Menambahkan nilai variable jumlah dengan angka deret
  deret--; // Mengubah nilai deret dengan mengurangi 1
  console.log('Jumlah saat ini: ' + jumlah)
}
console.log(jumlah);

console.log('\n')

//Contoh Looping For-loop 1
console.log('Contoh Looping For-loop 1')

for(var angka = 1; angka < 10; angka++) {
    console.log('Iterasi ke-' + angka);
} 

console.log('\n')

//Contoh Looping For-loop 2
console.log('Contoh Looping For-loop 2')

var jumlah = 0;
for(var deret = 5; deret > 0; deret--) {
  jumlah += deret;
  console.log('Jumlah saat ini: ' + jumlah);
}
console.log('Jumlah terakhir: ' + jumlah);

console.log('\n')

//Contoh Looping For-loop 3
console.log('Contoh Looping For-loop 3')

for(var deret = 0; deret < 10; deret += 2) {
    console.log('Iterasi dengan Increment counter 2: ' + deret);
}
   
console.log('-------------------------------');
   
for(var deret = 15; deret > 0; deret -= 3) {
    console.log('Iterasi dengan Decrement counter : ' + deret);
} 

console.log('\n')
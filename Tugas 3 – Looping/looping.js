//No. 1 Looping While
console.log('No. 1 Looping While')

console.log('LOOPING PERTAMA')
var flag = 1;
while(flag <= 10) {
        
    deret=flag*2;
    console.log(deret +' - I love coding');
    flag++;
    
}

console.log('LOOPING KEDUA')
var flag = 10;
while(flag >= 1) {
        
    deret=flag*2;
    console.log(deret +' - I will become a mobile developer');
    flag--;
    
}
console.log('\n')

//No. 2 Looping menggunakan for
console.log('No. 2 Looping menggunakan for')

for(var angka = 1; angka <=20; angka++) {
    if(angka%3 == 0 && angka%2 == 1){
        console.log(angka + ' I Love Coding');
    }else if(angka%2==1){
        console.log(angka + ' Santai');
    }else if(angka%2 == 0){
        console.log(angka + ' Berkualitas');
    }
    
} 
console.log('\n')

//No. 3 Membuat Persegi Panjang
console.log('No. 3 Membuat Persegi Panjang')

var kotak ='';
for(var i = 1; i <=4; i++) {
    for(var j = 1; j <=8; j++) {
        kotak +='#';
    }
        kotak +='\n';
}
console.log(kotak);

//No. 4 Membuat Tangga
console.log('No. 4 Membuat Tangga')

var row = 7;
for (var i = 1; i <= row; i++) {
	var result = '';
	for (var j = 1; j <= i; j++) {
	    result += '#';
	}
	console.log(result);
}
console.log('\n')

//No. 5 Membuat Papan Catur
console.log('No. 5 Membuat Papan Catur')
var kotak ='';
for(var i = 1; i <=8; i++) {
    if (i%2==0){
        for(var j = 1; j <=8; j++) {
            if (j%2==0){    
                kotak +='#';
            }else if(j%2==1){
                kotak +=' ';
            }
        }
        kotak +='\n';
    }else if(i%2==1){
        for(var j = 1; j <=8; j++) {
            if (j%2==0){    
                kotak +=' ';
            }else if(j%2==1){
                kotak +='#';
            }
        }
        kotak +='\n';
    }
    
}
console.log(kotak);
console.log('\n')
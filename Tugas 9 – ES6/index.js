//Let + Const
console.log("Let + Const");
//Normal Javascript:
console.log("Normal Javascript:");
var x = 1;

if (x === 1) {
  var x = 2;

  console.log(x);
  // expected output: 2
}
console.log(x); // 2

//ES6 :
console.log("ES6 :");
let y = 1;

if (y === 1) {
  let y = 2;

  console.log(y);
  // expected output: 2
}

console.log(y); // 1
const number = 42;
//number = 100; // Uncaught TypeError: Assignment to constant variable.
console.log("\n");

//Arrow Functions
console.log("Arrow Functions");
//Normal Javascript:
console.log("Normal Javascript:");
function f() {
  // isi Function
}

var f = function () {
  // isi function
};
//ES6 :
console.log("ES6 :");
var f = () => {
  //function
  return () => {
    //returning a function
  };
};
console.log("\n");

//Default Parameters
console.log("Default Parameters");
function multiply(a, b = 1) {
  return a * b;
}

console.log(multiply(5, 2));
// expected output: 10

console.log(multiply(5));
// expected output: 5
console.log("\n");

//Destructuring
console.log("Destructuring");
//Normal Javascript:
console.log("Normal Javascript:");
let studentName1 = {
  firstName1: "Peter",
  lastName1: "Parker",
};

const firstName1 = studentName1.firstName1;
const lastName1 = studentName1.lastName1;
//ES6 :
console.log("ES6 :");
let studentName2 = {
  firstName2: "Peter",
  lastName2: "Parker",
};

const { firstName2, lastName2 } = studentName2;

console.log(firstName2); // Peter
console.log(lastName2); // Parker
console.log("\n");

//Rest Parameters + Spread Operator
console.log("Rest Parameters + Spread Operator");
// Rest Parameters

let scores = ["98", "95", "93", "90", "87", "85"];
let [first, second, third, ...restOfScores] = scores;

console.log(first); // 98
console.log(second); // 95
console.log(third); // 93
console.log(restOfScores); // [90, 87, 85]
console.log("\n");

console.log("=================================================");
let array1 = ["one", "two"];
let array2 = ["three", "four"];
let array3 = ["five", "six"];

// ES5 Way / Normal Javascript

var combinedArray1 = array1.concat(array2).concat(array3);
console.log(combinedArray1); // ['one', 'two', 'three', 'four', 'five', 'six']

// ES6 Way

let combinedArray2 = [...array1, ...array2, ...array3];
console.log(combinedArray2); // ['one', 'two', 'three', 'four', 'five', 'six']
console.log("\n");

//Enhanced object literals
console.log("Enhanced object literals");
const fullName = "Zell Liew";

//Normal Javascript:
const Zell1 = {
  fullName: fullName,
};

// ES6 way
const Zell2 = {
  fullName,
};

console.log(Zell1);
console.log(Zell2);
console.log("\n");

//Template Literals
console.log("Template Literals");
const firstName = "Zell";
const lastName = "Liew";
const teamName = "unaffiliated";

const theString = `${firstName} ${lastName}, ${teamName}`;

console.log(theString); // Zell Liew, unaffiliated
console.log("\n");

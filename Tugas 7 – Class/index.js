//Deklarasi Class
/*
console.log("Deklarasi Class");
class Car {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
    this.sound = "honk! honk!vroomvroom";
  }
}
*/

//Ekspresi Class
console.log("Ekspresi Class");
// Tidak diberi nama
var Car = class {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
  }
};

console.log(Car.name); // Car

// Diberi nama
var Car = class Car2 {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
  }
};
console.log(Car.name); // Car2
console.log("\n");

//Method1
console.log("Method1");
class Car3 {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return "I have a " + this.carname;
  }
}

mycar = new Car3("Ford");
console.log(mycar.present()); // I have a Ford
console.log("\n");

//Method2
console.log("Method2");
class Car4 {
  constructor(brand) {
    this.carname = brand;
  }
  present(x) {
    return x + ", I have a " + this.carname;
  }
}

mycar = new Car4("Ford");
console.log(mycar.present("Hello"));
console.log("\n");

//Static Method
console.log("Static Method");
class Car5 {
  constructor(brand) {
    this.carname = brand;
  }
  static hello() {
    return "Hello!!";
  }
}

mycar = new Car5("Ford");

// memanggil 'hello()' pada class Car:
console.log(Car5.hello());
// dan tidak bisa pada 'mycar':
//console.log(mycar.hello());
// jika menggunakan sintaks tersebut akan memunculkan error.
console.log("\n");

//Inheritance
console.log("Inheritance");
class Car6 {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return "I have a " + this.carname;
  }
}

class Model extends Car6 {
  constructor(brand, mod) {
    super(brand);
    this.model = mod;
  }
  show() {
    return this.present() + ", it is a " + this.model;
  }
}

mycar = new Model("Ford", "Mustang");
console.log(mycar.show());
console.log("\n");

//Getters dan Setters
console.log("Getters dan Setters");
class Car7 {
  constructor(brand) {
    this.carname = brand;
  }
  get cnam() {
    return this.carname;
  }
  set cnam(x) {
    this.carname = x;
  }
}

mycar = new Car7("Ford");
console.log(mycar.cnam); // Ford
//getter cnam digunakan tanpa "()"
console.log("\n");

//method dengan property
console.log("method dengan property");
class Car8 {
  constructor(brand) {
    this._carname = brand;
  }
  get carname() {
    return this._carname;
  }
  set carname(x) {
    this._carname = x;
  }
}

mycar = new Car8("Ford");
mycar.carname = "Volvo"; // memanggil setter, mengubah Ford menjadi Volvo
console.log(mycar.carname); // Volvo

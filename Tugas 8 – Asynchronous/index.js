// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let times = 10000;

//CARA KE-1
/*
readBooks(times, books[0], function (sisawaktu) {
  readBooks(sisawaktu, books[1], function (sisawaktu) {
    readBooks(sisawaktu, books[2], function () {});
  });
});
*/

//CARA KE-2
/*
let i = 0;
function Read(times) {
  if (i <= books.length) {
    if (times >= books[i].timeSpent) {
      i++;
      readBooks(times, books[i], Read);
    }
  }
}
readBooks(times, books[i], Read);
*/

//CARA KE-3
let index = 0;
function Baca(waktu) {
  if (index < books.length) {
    readBooks(waktu, books[index], (sisaWaktu) => Baca(sisaWaktu));
  }
  index++;
}

Baca(times);

var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

//CARA KE-1
/*
let times = 10000;

let i = 0;
function BacaBuku(times, book) {
  readBooksPromise(times, book)
    .then(function (times) {
      i++;
      if (i < books.length) {
        BacaBuku(times, books[i]);
      }
    })
    .catch(function (error) {
      console.log(error);
    });
}
BacaBuku(times, books[i]);
*/

//CARA KE-2
let index = 0;
function Baca(times) {
  if (index < books.length) {
    readBooksPromise(times, books[index])
      .then((sisaWaktu) => Baca(sisaWaktu))
      .catch(Error);
  }
  index++;
}
Baca(10000);

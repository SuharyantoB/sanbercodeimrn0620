import React from "react";
/**LATIHAN */
//import Default from "./Latihan/Component/default";
//import ViewComponent from "./Latihan/Component/ViewComponent";
//import TextComponent from "./Latihan/Component/TextComponent";
//import TextInputComponent from "./Latihan/Component/TextInputComponent";
//import StylingFlexbox from "./Latihan/StylingFlexbox/index";
//import APILifecycle from "./Latihan/APILifecycle/APILifecycle";
//import ReactNavigation from "./Latihan/ReactNavigation/index";
// import Api from "./Latihan/RESTfulAPI/api";

/**TUGAS 12 */
//import YoutubeUI from "./Tugas/Tugas12/App";

/**TUGAS 13 */
//import Register from "./Tugas/Tugas13/RegisterScreen";
//import Login from "./Tugas/Tugas13/LoginScreen";
//import About from "./Tugas/Tugas13/AboutScreen";

/**TUGAS 14 */
//import Todo from "./Tugas/Tugas14/App";
//import Skill from "./Tugas/Tugas14/SkillScreen";

/**TUGAS 15 */
//import Navigasi from "./Tugas/Tugas15/index";
//import NavigasiReal from "./Tugas/TugasNavigation/index";

/**Quiz 3 */
import Quiz from "./Quiz3/index";

export default function App() {
  return (
    //Tugas 12
    //<YoutubeUI />

    //Tugas 13
    //<Register />
    //<Login />
    //<About />

    //Tugas 14
    //<Todo />
    //<Skill />

    //Tugas 15
    //<Navigasi />
    //<NavigasiReal />

    //Quiz 3
    <Quiz />

    //Latihan
    //<StylingFlexbox />
    //<Default />
    //<ViewComponent />
    //<TextComponent />
    //<TextInputComponent />
    //<APILifecycle />
    //<ReactNavigation />
    // <Api />
  );
}

//Deklarasi Object
console.log("Deklarasi Object");
var personArr = ["John", "Doe", "male", 27];
var personObj = {
  firstName: "John",
  lastName: "Doe",
  gender: "male",
  age: 27,
};

console.log(personArr);
console.log(personObj);
console.log("\n");

//Mengakses Nilai pada Object
console.log("Mengakses Nilai pada Object");
var motorcycle1 = {
  brand: "Handa",
  type: "CUB",
  price: 1000,
};
console.log(motorcycle1.brand); // "Handa"
console.log(motorcycle1.type); // "CUB"
console.log(motorcycle1["price"]);
console.log("\n");

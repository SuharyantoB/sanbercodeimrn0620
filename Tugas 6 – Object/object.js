//Soal No. 1 (Array to Object)
console.log("Soal No. 1 (Array to Object)");
var now = new Date();
var thisYear = now.getFullYear(); // 2020 (tahun sekarang)

function arrayToObject(arr) {
  if (!arr.length) {
    console.log("");
  } else {
    for (let i = 0; i < arr.length; i++) {
      let age = thisYear - arr[i][3];
      if (!arr[i][3] || arr[i][3] > thisYear) age = "Invalid Birth Year";
      let personObj = {
        firstName: arr[i][0],
        lastName: arr[i][1],
        gender: arr[i][2],
        age: age,
      };
      console.log(i + 1, arr[i][1], ":", personObj);
    }
  }
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];

arrayToObject(people);
console.log("\n");
arrayToObject(people2);
console.log("\n");
arrayToObject([]);
console.log("\n");

//Soal No. 2 (Shopping Time)
console.log("Soal No. 2 (Shopping Time)");
function shoppingTime(memberId, money) {
  var TokoX = {
    "Sepatu Stacattu": 1500000,
    "Baju Zoro": 500000,
    "Baju H&N": 250000,
    "Sweater Uniklooh": 175000,
    "Casing Handphone": 50000,
  };
  var harga = Object.values(TokoX).sort(function (a, b) {
    return b - a;
  });
  var listPurchased = [];
  var total = 0;
  for (var i = 0; i < harga.length; i++) {
    if (harga[i] <= money - total) {
      total += harga[i];
      listPurchased.push(
        Object.keys(TokoX)[Object.values(TokoX).indexOf(harga[i])]
      );
    }
  }

  var changeMoney = money - total;

  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  }

  var Shopping = {
    memberId: memberId,
    money: money,
    listPurchased: listPurchased,
    changeMoney: changeMoney,
  };
  return Shopping;
}
console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());
console.log("\n");

//Soal No. 3 (Naik Angkot)
console.log("Soal No. 3 (Naik Angkot)");
function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];

  var result = [];
  for (i = 0; i < arrPenumpang.length; i++) {
    var angkot = {
      penumpang: arrPenumpang[i][0],
      naikDari: arrPenumpang[i][1],
      tujuan: arrPenumpang[i][2],
      bayar: 8000,
    };

    for (var j = 0; j < rute.length; j++) {
      if (rute[j] == angkot.naikDari) {
        var awal = j;
      }
      if (rute[j] == angkot.tujuan) {
        var akhir = j;
      }
    }
    angkot.bayar = (akhir - awal) * 2000;
    result.push(angkot);
  }
  return result;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
console.log("\n");
